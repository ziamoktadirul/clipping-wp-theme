
(function($){
$(document).ready(function(){
	$('.menu').slicknav();
$(".testimonial_list").owlCarousel({
		singleItem: true,
		pagination: true,
		theme: "rrf-angle-paged"
	});
$(".jslider").owlCarousel({
		singleItem: true,
		pagination: true,
		theme: "rrf-angle-paged",
		autoPlay:true,
		items:1,
		responsive: true,
	});


$(".project_list").mixItUp();

$.scrollUp({
	scrollName: 'scrollUp', // Element ID
    topDistance: '300', // Distance from top before showing element (px)
    topSpeed: 300, // Speed back to top (ms)
    animation: 'fade', // Fade, slide, none
    animationInSpeed: 200, // Animation in speed (ms)
    animationOutSpeed: 200, // Animation out speed (ms)
    scrollText: false, // Text for element
    activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	
});

});
})(jQuery);