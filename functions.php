<?php
require_once('metabox/init.php');
require_once('metabox/custom.php');

if( file_exists(dirname(__FILE__)."/inc/ReduxCore/framework.php") ){
	require_once( dirname(__FILE__)."/inc/ReduxCore/framework.php" );
}

if( file_exists(dirname(__FILE__)."/inc/sample/config.php") ){
	require_once( dirname(__FILE__)."/inc/sample/config.php" );
	
}
	
if(!function_exists('clipping_setup')){
	
	function clipping_setup(){
		load_theme_textdomain( 'twentysixteen', get_template_directory() . '/languages' );
		
		add_theme_support( 'automatic-feed-links' );
		
		add_theme_support( 'title-tag' );
		add_image_size('portfo', 320,250);
		add_theme_support( 'post-thumbnails' );
		
		add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
		
		add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );	
		
	register_nav_menu('main-menu', __('Main Menu', 'clipping'));
		
		
		
	}
	
	register_post_type('Enterprise', array(
		'labels' => array(
			'name' => 'Enterprise',
			'add new' => 'Add New Enterprise',
			'add new item' => 'add new Enterprise'
		),
		'public' => true,
		'supports' => array('title', 'editor',)
	));
	register_post_type('Premium', array(
		'labels' => array(
			'name' => 'Premiums',
			'add new' => 'Add New Premium',
			'add new item' => 'add new Premium'
		),
		'public' => true,
		'supports' => array('title', 'editor', )
	));
	
	register_post_type('developer', array(
		'labels' => array(
			'name' => 'developers',
			'add new' => 'Add New developer',
			'add new item' => 'add new developer'
		),
		'public' => true,
		'supports' => array( 'title', 'editor',)
	));
	
	register_post_type('Starter', array(
		'labels' => array(
			'name' => 'Starters',
			'add new' => 'Add New Starter',
			'add new item' => 'add new Starter'
		),
		'public' => true,
		'supports' => array('title', 'editor',)
	));
	
	register_post_type('testimonial', array(
		'labels' => array(
			'name' => 'testimonials',
			'add new' => 'Add New testimonial',
			'add new item' => 'add new testimonial'
		),
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	));
	
	
	register_post_type('slider', array(
		'labels' => array(
			'name' => 'sliders',
			'add new' => 'Add New Slider',
			'add new item' => 'add new slider'
		),
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	));
	
register_post_type('portfolio', array(
		'labels' => array(
			'name' => 'portfolio',
			'add new' => 'Add New portfolio',
			'add new item' => 'add new portfolio'
		),
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	));

	register_taxonomy('portfolio-cat', 'portfolio', array(
		'labels' => array(
		'name' => 'Categories',
		'add new item' => 'add new category',
		
		),
		'public'=> true,
		'hierarchical'=> true,
		
	
	));
	
	
}

add_action('after_setup_theme', 'clipping_setup');



function clipping_style(){
	wp_enqueue_style('normalize', get_template_directory_uri().'/css/normalize.css');
	wp_enqueue_style('main', get_template_directory_uri().'/css/main.css');
	wp_enqueue_style('slicknav', get_template_directory_uri().'/css/slicknav.min.css');
	wp_enqueue_style('smart-pricing', get_template_directory_uri().'/css/smart-pricing.css');
	wp_enqueue_style('imagecss', get_template_directory_uri().'/css/image.css');
	wp_enqueue_style('rrf-carousel', get_template_directory_uri().'/css/rrf-carousel.css');
	wp_enqueue_style('carousel', get_template_directory_uri().'/css/owl.carousel.css');
	wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css');
	wp_enqueue_style('stylesheet', get_stylesheet_uri());
	wp_enqueue_style('responsive', get_template_directory_uri().'/css/responsive.css');
}
add_action('wp_enqueue_scripts', 'clipping_style');



function clipping_scripts(){
	
	wp_enqueue_script('modernizr', get_template_directory_uri().'/js/modernizr-2.8.3.min.js', array('jquery'));
	wp_enqueue_script('mixitup', get_template_directory_uri().'/js/jquery.mixitup.min.js', array('jquery'));
	wp_enqueue_script('scrollUp', get_template_directory_uri().'/js/jquery.scrollUp.min.js', array('jquery'));
	wp_enqueue_script('slicknav', get_template_directory_uri().'/js/jquery.slicknav.min.js', array('jquery'));
	wp_enqueue_script('carousel', get_template_directory_uri().'/js/owl.carousel.min.js', array('jquery'));
	wp_enqueue_script('bootstrapjs', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'));
	wp_enqueue_script('mainjs', get_template_directory_uri().'/js/main.js', array('jquery'));






}
add_action('wp_enqueue_scripts', 'clipping_scripts');


function default_menu(){

	echo '<ul>';
	echo '<li><a href="'.home_url().'">home</a></li>';
	echo '</ul>';
	
	
}

add_action('widgets_init', 'right_sidebar_widgets');

function right_sidebar_widgets(){
	register_sidebar(array(
		'name' => __('Left Sidebar', 'clipping'),
		'id' => 'clipping-left-sidebar',
		'description' => __('clipping Sidebar', 'clipping'),
		'before_widget' => '<div class="left-sidebar"> ',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name' => __('Right Sidebar', 'clipping'),
		'id' => 'clipping-right-sidebar',
		'description' => __('clipping Right Sidebar', 'clipping'),
		'before_widget' => '<div class="left-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	


register_sidebar(array(
		'name' => __('Service left Sidebar', 'clipping'),
		'id' => 'Service-left-sidebar',
		'description' => __('Service Left Sidebar', 'clipping'),
		'before_widget' => '<div class="left-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

register_sidebar(array(
		'name' => __('Service Right sidebar', 'clipping'),
		'id' => 'Service-right-sidebar',
		'description' => __('Service Right Sidebar', 'clipping'),
		'before_widget' => '<div class="left-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

register_sidebar(array(
		'name' => __('Payment Sidebar', 'clipping'),
		'id' => 'payment-sidebar',
		'description' => __('payment sidebar', 'clipping'),
		'before_widget' => '<div class="left-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

register_sidebar(array(
		'name' => __('free Sidebar', 'clipping'),
		'id' => 'free-sidebar',
		'description' => __('Free trail sidebar', 'clipping'),
		'before_widget' => '<div class="left-sidebar">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));



}


