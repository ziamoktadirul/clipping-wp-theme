<?php get_header(); ?>
		
		
		
		<!--- slider Section Start -->
	<?php get_template_part('slider'); ?>

		<!--- slider Section End-->
		
		<!--- Sidebar Section Start -->
		
		<div class="main-content-area"> 
		<div class="mid">
			<div class="container"> 
				<div class="row"> 
					<div class="col-md-3">
					<?php dynamic_sidebar('clipping-left-sidebar'); ?>
					
					
					</div>
					<div class="col-md-6">
						<div class="pragrap"> 
							<h3><?php echo $clipping['pro_hes']; ?></h3>
							<p><?php echo $clipping['pro_container']; ?></p>
						</div>
					</div>
					<div class="col-md-3">
				<?php dynamic_sidebar('clipping-right-sidebar'); ?>
					</div>
				
				</div>
			
			</div>
			</div>
		</div>
		<div class="servioffer"> 
		<div class="mid">
			<div class="container"> 
				<div class="row"> 
				
					<div class="col-md-3"> 
					<?php dynamic_sidebar('Service-left-sidebar'); ?>
					
					</div>
					<div class="col-md-6"> 
						<div class="sercon"> 
						<h2><?php echo $clipping['service_head']; ?></h2>
						<p><?php echo $clipping['service_content']; ?></p>
						</div>
					</div>
					<div class="col-md-3"> 
					<?php dynamic_sidebar('Service-right-sidebar'); ?>
				
					</div>
					
					
				</div>
			</div>
			</div>
		</div>
		<div class="path-image"> 
		<div class="mid">
			<div class="container"> 
				<div class="row pathimage"> 
					<h2><?php echo $clipping['path_header']; ?></h2>
					<div class="col-md-12"> 
						<div class="project_filter"> 
							<ul class="project_menu">
								<li class="filter" data-filter="all">all</li>
								<?php
								$categories = get_terms(array('portfolio-cat') );
								
								foreach($categories as $categori): ?>
								<li class="filter" data-filter=".<?php echo $categori->slug; ?>"><?php echo $categori->name; ?></li>
								
								<?php endforeach;?>
								
								
							</ul>
						</div>
						<div class="project_list"> 
						<?php 
							$port = new WP_Query(array(
							'post_type' => 'portfolio',
						
						));
						while
					
						
						
						($port->have_posts()) : $port->the_post(); ?>
							<div class="mix 
							<?php 
							$categoriesc = get_the_terms(get_the_id(), 'portfolio-cat');
			
							foreach($categoriesc as $categori): ?>
							<?php echo $categori->slug; ?> 
					
							<?php endforeach; ?>
							
							single-project" data-my-order="1">
								<?php the_post_thumbnail('portfo'); ?>
							</div>
							
						<?php endwhile; ?>
						
						
						
							
						
						</div>
						
					</div>


					
				</div>
			</div>
			</div>
		</div>
		
		<div class="testimonial-area"> 
		<div class="mid">
			<div class="container"> 
				<div class="row testi"> 
					<h2>Testimonial</h2>
					<div class="col-md-12"> 
						<div class="testimonial_list">
						<?php

					$testimo = new WP_Query(array(
						'post_type'=> 'testimonial',
					
					));


						while($testimo->have_posts()): $testimo->the_post(); ?>
					<div class="single_testimonial">
						<?php the_post_thumbnail(); ?>
						<p><?php the_content(); ?></p>
						
						<h3><?php the_title(); ?></h3>
					</div>
					<?php endwhile; ?>
			
			
			
				</div>
			</div>
		</div>
					</div>
					
					</div>
					
					
				</div>
			</div>
		</div>
		

		<?php get_footer(); ?>
