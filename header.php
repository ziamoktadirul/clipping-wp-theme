

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
    
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 

<?php global $clipping;?>

<?php wp_head(); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<div class="header-area"> 
			<div class="mid"> 
				<div class="containter"> 
					<div class="row"> 
						<div class="col-md-3">
							<div class="logo"> 
								<a href="<?php home_url(); ?>"><img src="<?php echo $clipping['Logo_upload']['url']; ?>" alt="" /></a>
							</div>
						</div>
						<div class="col-md-6">
							<div class="banner"> 
								<img src="<?php echo $clipping['baner_upload']['url']; ?>" alt="" />
							</div>
						</div>
						<div class="col-md-3">
							<div class="conta"> 
								<p><?php echo $clipping['ema_il']; ?></p>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		
		</div>

		<div class="main-menu-area"> 
			<div class="mid"> 
			
				
							 <div class="menu"> 
							
								<?php 
					wp_nav_menu(
						array(
							'theme_location' => 'main-menu',
							'fallback_cb' => 'default_menu',
							
							
						)
					);
				
				?>
							
								
							</div>
		
			</div>
		
		</div>
		
		