<?php

/*


Template Name: Service

*/


 get_header(); ?>
		<!--- slider Section End-->
		
		<!--- Sidebar Section Start -->
		
		<div class="main-content-area"> 
		<div class="mid">
			<div class="container"> 
				<div class="row"> 
					<div class="col-md-3">
				<?php dynamic_sidebar('clipping-left-sidebar'); ?>
						<div class="left-sidebar"> 
						<?php dynamic_sidebar('payment-sidebar'); ?>
						
						
						</div>
						
					</div>
					<div class="col-md-6">
						<div class="pragrap"> 
						<h3><?php echo $clipping['servi_tit']; ?></h3>
							<p><?php echo $clipping['servi_content']; ?></p>
						
						
						
						
						<?php echo $clipping['servi_content_body']; ?>
						
							
							
						</div>
					</div>
					<div class="col-md-3">
					<?php dynamic_sidebar('clipping-right-sidebar'); ?>
						<div class="left-sidebar"> 
							
							<?php dynamic_sidebar('free-sidebar'); ?>
						
						</div>
						
					</div>
				
				</div>
			
			</div>
			</div>
		</div>
		
		
		
		<div class="testimonial-area"> 
		<div class="mid">
			<div class="container"> 
				<div class="row testi"> 
					<h2><?php echo $clipping['testi_header']; ?></h2>
					<div class="col-md-12"> 
						<div class="testimonial_list">
						<?php

					$testimo = new WP_Query(array(
						'post_type'=> 'testimonial',
					
					));


						while($testimo->have_posts()): $testimo->the_post(); ?>
					<div class="single_testimonial">
						<?php the_post_thumbnail(); ?>
						<p><?php the_content(); ?></p>
						
						<h3><?php the_title(); ?></h3>
					</div>
					<?php endwhile; ?>
			
			
			
				</div>
			</div>
		</div>
					</div>
					
					</div>
					
					
				</div>
			</div>
		</div>
		
		
		<?php get_footer(); ?>