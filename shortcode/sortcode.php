<?php


add_shortcode('slider', 'cliping_slider');


function cliping_slider($attr, $content){
	ob_start();
	
	extract(shortcode_atts(array(
	$image1 => get_template_directory_uri().'/img/slider.jpg',
	$image2 => get_template_directory_uri().'/img/slider2.jpg',
	$image3 => get_template_directory_uri().'/img/slider3.jpg',
	$image4 => get_template_directory_uri().'/img/slider4.jpg',
	$image5 => get_template_directory_uri().'/img/slider5.jpg',
	
	
	), $attr));
	
	?>
	
<section class="slider"> 
			<div class="mid">
			
			
				<div class="jslider"> 
				
				<?php 
					$photo1 = wp_get_attachment_image_src($image1, 'full');
					$photo2 = wp_get_attachment_image_src($image2, 'full');
					$photo3 = wp_get_attachment_image_src($image3, 'full');
					$photo4 = wp_get_attachment_image_src($image4, 'full');
					$photo5 = wp_get_attachment_image_src($image5, 'full');
				?>
				
				
					<img src="<?php echo $photo1[0]; ?>" alt="images" />
					<img src="<?php echo $photo2[0]; ?>" alt="images" />
					<img src="<?php echo $photo3[0]; ?>" alt="images" />
					<img src="<?php echo $photo4[0]; ?>" alt="images" />
					<img src="<?php echo $photo5[0]; ?>" alt="images" />
				</div>
				
			</div>
		</section>
	
	
	<?php return ob_get_clean();
	
}




