<?php 


/*

Template Name:pricing


*/

global $clipping;

get_header(); ?>
		
		<div class="main-content-area"> 
		<div class="mid">
			<div class="container"> 
				<div class="col-md-12">
	<div class="smart-grids">

        <div class="smart-wrapper">
            <div class="smart-pricing">
                <div class="pricing-tables elegant-style four-colm">
                    <div class="colm">
                        <div class="colm-list">
						
						
                            <div class="pricing-header header-colored">
                                <h1 class="monoblack-top-1"><?php echo $clipping['start_title']; ?></h1>
                                <h2 class="monoblack-bottom-1"><span><?php echo $clipping['start_dolar']; ?></span><?php echo $clipping['start_dolar_text']; ?></h2>
                                <div class="ribbon-large"> 
                                	<div class="ribbon-inner ribbon-inner-blue"><?php echo $clipping['start_dolar_ribon']; ?></div> 
                                </div><!-- end .ribbon-large section -->                                
                            </div><!-- end .pricing-header section -->
							 <ul>
							<?php 
						
						$star = new WP_Query(array(
							'post_type' => 'Starter',
						
						
						));
						
						
						
						
						
						while($star->have_posts()): $star->the_post(); ?>
                           
                                <li><?php the_content(); ?></li>
                               
                           
							 <?php endwhile; ?>
							  </ul>
                            <div class="pricing-footer">
                                <button type="button" class="pricing-button monoblack1-btn"> Sign up </button>
                           
						  
						  </div><!-- end .pricing-footer section -->  
						 
                        </div><!-- end .colm-list section -->
                    </div><!-- end .colm section -->
                    <div class="colm">
                        <div class="colm-list">
                            <div class="pricing-header header-colored">
                                <h1 class="monoblack-top-2"><?php echo $clipping['dev_title']; ?></h1>
                                <h2 class="monoblack-bottom-2"><span><?php echo $clipping['dev_dolar']; ?></span><?php echo $clipping['dev_dolar_text']; ?></h2>
                                <div class="ribbon-large"> 
                                	<div class="ribbon-inner ribbon-inner-yellow"><?php echo $clipping['dev_ribon']; ?></div> 
                                </div><!-- end .ribbon-large section -->                                
                            </div><!-- end .pricing-header section -->
                            <ul>
							<?php

								$devlo = new WP_Query(array(
								'post_type' => 'developer'
								
								));


							while($devlo->have_posts()): $devlo->the_post(); ?>
                                <li><?php the_content(); ?></li>
                          
								<?php endwhile; ?>
                            </ul>
                            <div class="pricing-footer">
                                <button type="button" class="pricing-button  monoblack2-btn"> Sign up </button>
                            </div><!-- end .pricing-footer section -->                                                                   	
                        </div><!-- end .colm-list section -->                
                    </div><!-- end .colm section -->
                    <div class="colm">
                        <div class="colm-list">
                            <div class="pricing-header header-colored">
                                <h1 class="monoblue-top-3"><?php echo $clipping['Pre_title']; ?></h1>
                                <h2 class="monoblue-bottom-3"><span><?php echo $clipping['Pre_dollar']; ?></span><?php echo $clipping['Pre_dollar_text']; ?></h2>
                                <div class="ribbon-large"> 
                                	<div class="ribbon-inner ribbon-inner-red"><?php echo $clipping['Pre_ribon']; ?></div> 
                                </div><!-- end .ribbon-large section -->
                            </div><!-- end .pricing-header section -->
                            <ul>
							<?php


					$premi = new WP_Query(array(
						'post_type' => 'Premium'
					));

							while($premi->have_posts()) : $premi->the_post(); ?>
                                <li><?php the_content(); ?></li>
                           
								<?php endwhile; ?>
                            </ul>
                            <div class="pricing-footer">
                                <button type="button" class="pricing-button  monoblue3-btn"> Sign up </button>
                            </div><!-- end .pricing-footer section -->                                 
                        </div><!-- end .colm-list section -->                
                    </div><!-- end .colm section -->
                    <div class="colm">
                        <div class="colm-list">
                            <div class="pricing-header header-colored">
                                <h1 class="monoblue-top-4"><?php echo $clipping['Enterprise_title']; ?></h1>
                                <h2 class="monoblue-bottom-4"><span><?php echo $clipping['Enterprise_dolar']; ?></span><?php echo $clipping['Enterprise_dolar_text']; ?></h2>
                                <div class="ribbon-large"> 
                                	<div class="ribbon-inner ribbon-inner-lite-green"><?php echo $clipping['Enterprise_ribon']; ?></div> 
                                </div><!-- end .ribbon-large section -->                                
                            </div><!-- end .pricing-header section -->
                            <ul>
							<?php 
							
							
							$enter = new WP_Query(array(
								'post_type' => 'Enterprise'
							));
							
							
							while($enter->have_posts()): $enter->the_post(); ?>
                                <li><?php the_content(); ?></li>
                               
								<?php endwhile; ?>
                            </ul>
                            <div class="pricing-footer">
                                <button type="button" class="pricing-button  monoblue4-btn"> Sign up </button>
                            </div><!-- end .pricing-footer section -->                                 
                        </div><!-- end .colm-list section -->                
                    </div><!-- end .colm section -->
                </div><!-- end .pricing-tables section -->
            </div><!-- end .smart-pricing section -->
        </div><!-- end .smart-wrapper section -->
        </div>
				
				</div>
			
			</div>
			</div>
		</div>
		
		
		
		<div class="testimonial-area"> 
		<div class="mid">
			<div class="container"> 
				<div class="row testi"> 
					<h2><?php echo $clipping['testi_header']; ?></h2>
					<div class="col-md-12"> 
						<div class="testimonial_list">
						<?php

					$testimo = new WP_Query(array(
						'post_type'=> 'testimonial',
					
					));


						while($testimo->have_posts()): $testimo->the_post(); ?>
					<div class="single_testimonial">
						<?php the_post_thumbnail(); ?>
						<p><?php the_content(); ?></p>
						
						<h3><?php the_title(); ?></h3>
					</div>
					<?php endwhile; ?>
			
			
			
				</div>
			</div>
		</div>
					</div>
					
					</div>
					
					
				</div>
			</div>
		</div>

		
		
		<?php get_footer(); ?>