<?php

add_action('cmb2_admin_init', 'pricing_table_metabox');


function pricing_table_metabox(){
	
	$stratermetabox = new_cmb2_box(array(
	
		'title' => 'Additional Field',
		'object_types' => array('Starter'),
		'id' => 'starter-fields'
	
	));
	
	$stratermetabox->add_field(array(
		'name' => 'strter header',
		'id' => 'start-field',
		'type' => 'text'
	
	));
	
}